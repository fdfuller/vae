import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm

from keras.layers import Input, Dense, Lambda
from keras.layers.merge import concatenate
from keras.models import Model
from keras import backend as K
from keras import metrics

N = 10000
M = 100
X = np.linspace(0,100,M)
data_sets = np.row_stack([np.random.poisson(lam=x,size=N) + np.random.normal(loc=0, scale=0.2, size=N) for x in X])
y = data_sets.reshape(N*M)
x = np.repeat(X,N)
D = np.column_stack([x,y])
np.random.shuffle(D)
D = D.T
test = D[:,-10000:]
train = D[:,:10000]
y_train = train[0,:]
y_test = train[0,:]
x_train = train[1,:]
x_test = train[1,:]

n_y = 1
n_x = 1
# n_y = y_train.shape[1]

m = 500   # minibatch size?
n_encoder = 50 #dimension of the encoder
n_z = 2  # dimention of latent space
n_decoder = n_encoder #dimension of the decoder
n_epoch = 100


# Q(z|X) -- encoder
x = Input(batch_shape=(m, n_x)) # size of fake images
cond = Input(batch_shape=(m, n_y))

inputs = concatenate([x, cond], axis=-1)
encoder = Dense(n_encoder, activation='relu', name='encoder')(inputs)
z_mean = Dense(n_z, name='latent_mean')(encoder)
z_log_var = Dense(n_z, name='latent_variance')(encoder)

def sample_z(args):
    z_mean, z_log_var = args
    eps = K.random_normal(shape=(m, n_z), mean=0., stddev=1.)
    return z_mean + K.exp(z_log_var / 2.) * eps

z = Lambda(sample_z)([z_mean, z_log_var])
z_cond = concatenate([z, cond], axis=-1) # <--- NEW!

decoder_hidden = Dense(n_decoder, activation='relu',name='hidden_decoder')
decoder_mean = Dense(n_x, activation='sigmoid',name='decoder')

xcond_decoded = decoder_hidden(z_cond)
xcond_decoded_mean = decoder_mean(xcond_decoded)

cvae = Model([x, cond], xcond_decoded_mean)
xent_loss = n_x * metrics.binary_crossentropy(x, xcond_decoded_mean)
kl_loss = - 0.5 * K.sum(1 + z_log_var - K.square(z_mean) - K.exp(z_log_var), axis=-1)
cvae_loss = K.mean(xent_loss + kl_loss)

cvae.add_loss(cvae_loss)
cvae.compile(optimizer='adam')
cvae.summary()

cvae.fit([x_train, y_train],
        shuffle=True,
        epochs=n_epoch,
        batch_size=m,
        validation_data=([x_test, y_test], None))

# build a model to project inputs on the latent space
encoder = Model([x, cond], z_mean)
encoder.save('encoder_linear.h5')

# build a digit generator that can sample from the learned distribution
gen_decoder_input = Input(shape=(n_z,))
gen_cond_input = Input(shape=(n_y,))
gen_inputs = concatenate([gen_decoder_input, gen_cond_input], axis=-1)
_h_decoded = decoder_hidden(gen_inputs) #this object is trained
_x_decoded_mean = decoder_mean(_h_decoded) #so is this one
generator = Model([gen_decoder_input,gen_cond_input], _x_decoded_mean)

generator.save('generator_linear.h5')
