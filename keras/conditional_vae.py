import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm

from keras.layers import Input, Dense, Lambda
from keras.layers.merge import concatenate
from keras.models import Model
from keras import backend as K
from keras import metrics
from keras.datasets import mnist
from sklearn.preprocessing import OneHotEncoder


# train the VAE on MNIST digits
(x_train, y_train), (x_test, y_test) = mnist.load_data()
x_train = x_train.astype('float32') / 255.
x_test = x_test.astype('float32') / 255.
y_test_old = np.copy(y_test)
# y_train = y_train.astype('float32') / 9.0
# y_test = y_test.astype('float32') / 9.0
g_train = OneHotEncoder(n_values=10, sparse=False, categorical_features = 'all', dtype=np.float32)
g_test = OneHotEncoder(n_values=10, sparse=False, categorical_features = 'all', dtype=np.float32)

y_train = g_train.fit_transform(np.expand_dims(y_train,1))
y_test = g_test.fit_transform(np.expand_dims(y_test,1))
x_train = x_train.reshape((len(x_train), np.prod(x_train.shape[1:])))
x_test = x_test.reshape((len(x_test), np.prod(x_test.shape[1:])))
n_x = x_train.shape[1] # 784
# n_y = 1
n_y = y_train.shape[1]

m = 50   # minibatch size?
n_encoder = 256 #dimension of the encoder
n_z = 2  # dimention of latent space
n_decoder = n_encoder #dimension of the decoder
n_epoch = 40


# Q(z|X) -- encoder
x = Input(batch_shape=(m, n_x)) # size of MNIST images
cond = Input(batch_shape=(m, n_y))

inputs = concatenate([x, cond], axis=-1)
encoder = Dense(n_encoder, activation='relu', name='encoder')(inputs)
z_mean = Dense(n_z, name='latent_mean')(encoder)
z_log_var = Dense(n_z, name='latent_variance')(encoder)

def sample_z(args):
    z_mean, z_log_var = args
    eps = K.random_normal(shape=(m, n_z), mean=0., stddev=1.)
    return z_mean + K.exp(z_log_var / 2.) * eps

z = Lambda(sample_z)([z_mean, z_log_var])
z_cond = concatenate([z, cond], axis=-1) # <--- NEW!

decoder_hidden = Dense(n_decoder, activation='relu',name='hidden_decoder')
decoder_mean = Dense(n_x, activation='sigmoid',name='decoder')

xcond_decoded = decoder_hidden(z_cond)
xcond_decoded_mean = decoder_mean(xcond_decoded)

cvae = Model([x, cond], xcond_decoded_mean)
xent_loss = n_x * metrics.binary_crossentropy(x, xcond_decoded_mean)
kl_loss = - 0.5 * K.sum(1 + z_log_var - K.square(z_mean) - K.exp(z_log_var), axis=-1)
cvae_loss = K.mean(xent_loss + kl_loss)

cvae.add_loss(cvae_loss)
cvae.compile(optimizer='rmsprop')
cvae.summary()

cvae.fit([x_train, y_train],
        shuffle=True,
        epochs=n_epoch,
        batch_size=m,
        validation_data=([x_test, y_test], None))

# build a model to project inputs on the latent space
encoder = Model([x, cond], z_mean)
encoder.save('digit_encoder.h5')

# # display a 2D plot of the digit classes in the latent space
# xcond_test_encoded = encoder.predict([x_test, y_test], batch_size=m)
# plt.figure(figsize=(6, 6))
# plt.scatter(xcond_test_encoded[:, 0], xcond_test_encoded[:, 1], c=y_test_old)
# plt.colorbar()
# plt.show()
# plt.savefig('real_encoded_clustering.png')

# build a digit generator that can sample from the learned distribution
gen_decoder_input = Input(shape=(n_z,))
gen_cond_input = Input(shape=(n_y,))
gen_inputs = concatenate([gen_decoder_input, gen_cond_input], axis=-1)
_h_decoded = decoder_hidden(gen_inputs) #this object is trained
_x_decoded_mean = decoder_mean(_h_decoded) #so is this one
generator = Model([gen_decoder_input,gen_cond_input], _x_decoded_mean)

generator.save('digit_generator.h5')
#
# # display a 2D manifold of the digits
# n = 15  # figure with 15x15 digits
# digit_size = 28
# figure = np.zeros((digit_size * n, digit_size * n))
# # linearly spaced coordinates on the unit square were transformed through the inverse CDF (ppf) of the Gaussian
# # to produce values of the latent variables z, since the prior of the latent space is Gaussian
# grid_x = norm.ppf(np.linspace(0.05, 0.95, n))
# grid_y = norm.ppf(np.linspace(0.05, 0.95, n))
# #
#
# pred_num = np.array([0.,0.,1.,0.,0.,0.,0.,0.,0.,0.])
# # pred_num = 3.
# for i, yi in enumerate(grid_x):
#     for j, xi in enumerate(grid_y):
#         z_sample = np.expand_dims(np.array([xi, yi]),0)
#         # cond_val = np.expand_dims(np.array([pred_num/9.]),0)
#         cond_val = np.expand_dims(pred_num,0)
#         x_decoded = generator.predict([z_sample, cond_val])
#         digit = x_decoded[0].reshape(digit_size, digit_size)
#         figure[i * digit_size: (i + 1) * digit_size,
#                j * digit_size: (j + 1) * digit_size] = digit
#
# plt.figure(figsize=(10, 10))
# plt.imshow(figure)
# plt.show()
# plt.savefig('real_encoded_conditional_twos.png')
