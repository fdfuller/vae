import numpy as np
from scipy.spatial.distance import cdist


def gaussian(x,mu,sigma):
    return np.exp(-(x - mu)**2/(2*float(sigma)**2))

def rbf_kernel(x,xp,theta):
    if len(x.shape)<2:
        x = np.matrix(x).T
    if len(xp.shape)<2:
        xp = np.matrix(xp).T
    r = np.exp(float(theta[0]))*np.exp(-cdist(x,xp,'sqeuclidean')/(2*np.exp(float(theta[1]))**2))
    r += 1E-6*np.eye(len(x))
    return r

def draw_fun(npoints,theta):
    axis = np.linspace(-1,1,npoints)
    kernel = rbf_kernel(axis,axis,np.array([np.log(theta[0]), np.log(theta[1])]))
    return _draw_fun(kernel)

def _draw_fun(kernel):
    return np.random.multivariate_normal(np.zeros(kernel.shape[0],dtype=kernel.dtype), kernel)

def draw_fun_2d(npoints,theta):
    axis = np.linspace(-1,1,int(npoints))
    axis2 = np.linspace(-1,1,int(npoints))
    (a,b) = np.meshgrid(axis,axis2)
    Xspace = np.column_stack([np.ravel(a),np.ravel(b)])
    r = np.random.multivariate_normal(0*Xspace[:,0],
                                  rbf_kernel(Xspace,Xspace,np.array([np.log(1.0), np.log(theta)])))
    return r.reshape((npoints,npoints))

def random_exciting_image(npoints,sigma,theta):
    x_axis = np.linspace(-1,1,npoints)
    x_fun = np.broadcast_to(np.expand_dims(gaussian(x_axis,0.0,sigma),0),(npoints,npoints))
    img = draw_fun_2d(npoints,theta)
    return (np.tanh(0.8*img)+1.0)*x_fun

def random_boring_image(npoints,sigma,sigma2):
    x_axis = np.linspace(-1,1,npoints)
    x_fun = np.broadcast_to(np.expand_dims(gaussian(x_axis,0.0,sigma),0),(npoints,npoints))
    x_fun2 = np.broadcast_to(np.expand_dims(gaussian(x_axis,0.0,sigma2),0),(npoints,npoints))
    return x_fun * x_fun2.T

def random_img_mixture(npoints,sigma,sigma2,theta,alpha):
    img1 = random_boring_image(npoints,sigma,sigma2)
    img2 = random_exciting_image(npoints,sigma,theta)
    r = img1 + alpha*img2
    return r/r.max()

def random_power(npoints):
    return np.random.gamma(1.5,scale=.3,size=(npoints,))

#force

N = 50000
L = 28
x = np.zeros((N,L,L),dtype=np.float32)
y = np.zeros((N,),dtype=np.float32)
for k in range(N):
    y[k] = random_power(1).astype(np.float32)
    x[k,:,:] = random_img_mixture(L,0.2,0.65,0.2,y[k]).astype(np.float32)
    print(k)


np.save('fake_images_bigger_more_messy.npy',x)
np.save('fake_powers_bigger_more_messy.npy',y)